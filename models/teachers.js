var mongoose = require("mongoose")
var Schema = mongoose.Schema
// mongoose.connect("mongodb://localhost/project")

let TeacherSchema = new Schema({
    name: String,
    age: Number,
    gender:{
        type:String,
        default:"Man"
    },
    grade: Number,
    rank: {
        type:String,
        default:"Lecture"
    },
    hobbies: String,
    upvotes:{type:Number,
        default:0}
})

module.exports = mongoose.model("Teacher", TeacherSchema)
