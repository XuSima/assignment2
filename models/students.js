var mongoose = require("mongoose")
var Schema = mongoose.Schema
// mongoose.connect("mongodb://localhost/project")


let StudentSchema = new Schema({
    name: String,
    age: Number,
    gender:{
        type:String,
        default:"Boy"
    },
    grade: Number,
    performance: {
        type:String,
        default:"Good"
    },
    hobbies: String,
    upvotes: {type: Number, default: 0}
})

module.exports = mongoose.model("Student", StudentSchema)



